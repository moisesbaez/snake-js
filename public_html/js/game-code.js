var context;

var snake;
var snakeLength;
var snakeSize;
var snakeDirection;

var food;
var foodPosition;

var screenWidth;
var screenHeight;

var gameState;

var startMenu;
var gameOverMenu;
var playHUD;
var scoreboard;

window.onload = function() {
    initialize();
    setInterval(gameLoop, 1000/30);
};

function initialize() {
    var canvas = document.getElementById("canvas");
    context = canvas.getContext("2d");
    
    screenWidth = window.innerWidth;
    screenHeight = window.innerHeight;
    
    canvas.width = screenWidth;
    canvas.height = screenHeight;
   
    getDOMElements();
    setEventListeners();
    
    setMenu(startMenu);
    setMenu(gameOverMenu);
    setState("MENU");
}

function getDOMElements() {
    startMenu = document.getElementById("startMenu");
    gameOverMenu = document.getElementById("gameOver");
    playHUD = document.getElementById("playHUD");
}

function setEventListeners() {
    document.addEventListener("keydown", keyboardHandler, false);
    startMenu.getElementsByClassName("startButton")[0].addEventListener("click", startGame, false);
    gameOverMenu.getElementsByClassName("restartButton")[0].addEventListener("click", restartGame, false);
}

function gameLoop() {
    gameDraw();
    drawScoreboard();
    if(gameState === "PLAY") {
        drawSnake();
        drawFood();
        updateSnake();
    }
    else if(gameState === "GAME_OVER") {
        drawSnake();
        drawFood();
    }
}

function gameDraw() {
    context.clearRect(0, 0, screenWidth, screenHeight);
    
    context.fillStyle = "rgb(123, 209, 159)";
    context.fillRect(0, 0, screenWidth, screenHeight);
}

function initializeSnake() {
    snake = [];
    snakeLength = 1;
    snakeSize = 15;
    snakeDirection = "right";
    
    for(var count = snakeLength - 1; count >= 0; count--) {
        snake.push({x: count, y: 0});
    }
}

function drawSnake() {
    for(var count = 0; count < snake.length; count++) {
        context.fillStyle = "white";
        context.fillRect(snake[count].x * snakeSize, snake[count].y * snakeSize, snakeSize, snakeSize);
    }
}

function updateSnake() {
    var snakeHeadX = snake[0].x;
    var snakeHeadY = snake[0].y;
    
    if(snakeDirection === "right") {
        snakeHeadX++;
    }
    else if(snakeDirection === "left") {
        snakeHeadX--;
    }
    else if(snakeDirection === "up") {
        snakeHeadY--;
    }
    else if(snakeDirection === "down") {
        snakeHeadY++;
    }
    
    checkCollisions(snakeHeadX, snakeHeadY);
    
    var snakeTail = snake.pop();
    
    snakeTail.x = snakeHeadX;
    snakeTail.y = snakeHeadY;
    
    snake.unshift(snakeTail);
}

function upgradeSnake() {
    snake.push({x: snakeLength, y: snake[snakeLength - 1]});
    snakeLength++;
}

function initializeFood() {
    foodPosition = {x: 0, y: 0};
    createFood();
}

function drawFood() {
    context.fillStyle = "white";
    context.fillRect(foodPosition.x * snakeSize, foodPosition.y * snakeSize, snakeSize, snakeSize);
}

function createFood() {
    var randomWidth = Math.floor(Math.random() * screenWidth + 1);
    var randomHeight = Math.floor(Math.random() * screenHeight + 1);
    
    foodPosition.x = Math.floor(randomWidth / snakeSize);
    foodPosition.y = Math.floor(randomHeight / snakeSize);
}

function checkFoodCollision(headX, headY) {
    if(headX === foodPosition.x && headY === foodPosition.y) {
        createFood();
        upgradeSnake();
    }
}

function checkWallCollision(headX, headY) {
    if(headX * snakeSize >= screenWidth || headX * snakeSize < 0) {
        setState("GAME_OVER");
    }
    else if(headY * snakeSize >= screenHeight || headY * snakeSize < 0) {
        setState("GAME_OVER");
    }
}

function checkSnakeCollision(headX, headY) {
    for(var count = 1; count < snake.length; count++) {
        if(headX === snake[count].x && headY === snake[count].y) {
            setState("GAME_OVER");
            return;
        }
    }
}

function checkCollisions(headX, headY) {
    checkFoodCollision(headX, headY);
    checkWallCollision(headX, headY);
    checkSnakeCollision(headX, headY);
}

function keyboardHandler(event) {
    var keyPressed = event.keyCode;
    
    if(keyPressed === 37 && snakeDirection !== "right") {
        snakeDirection = "left";
    }
    else if(keyPressed === 39 && snakeDirection !== "left") {
        snakeDirection = "right";
    }
    else if(keyPressed === 38 && snakeDirection !== "down") {
        snakeDirection = "up";
    }
    else if(keyPressed === 40 && snakeDirection !== "up") {
        snakeDirection = "down";
    }
}

function setScoreboard() {
    scoreboard = playHUD.getElementsByClassName("scoreboard")[0];
    
    playHUD.style.top = "0px";
    playHUD.style.left = "0px";
}

function drawScoreboard() {
    scoreboard.innerHTML = "Length: " + snakeLength;
}

function setMenu(menu) {
    menu.style.top = (screenHeight * 0.5) - (menu.offsetHeight * 0.5) + "px";
    menu.style.left = (screenWidth * 0.5) - (menu.offsetWidth * 0.5) + "px";
}

function removeMenu(menu) {
    menu.style.visibility = "hidden";
}

function displayMenu(menu) {
    menu.style.visibility = "visible";
}

function setState(state) {
    gameState = state;
    showMenu(state);
}

function showMenu(state) {
    if (state === "GAME_OVER") {
        displayMenu(gameOverMenu);
    }
    else if (state === "MENU") {
        displayMenu(startMenu);
    }
    else if (state === "PLAY") {
        displayMenu(playHUD);
    }
}

function resetGame() {
    initializeSnake();
    initializeFood();
}

function startGame() {
    resetGame();
    removeMenu(startMenu);
    setScoreboard();
    setState("PLAY");
}

function restartGame() {
    resetGame();
    removeMenu(gameOverMenu);
    setState("PLAY");
}